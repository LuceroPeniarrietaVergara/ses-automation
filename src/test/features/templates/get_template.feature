Feature: Template - GET

  Scenario: get/templates
    When execute the [GET] method for 'templates' api:
      | Params       | Value |
      | page[number] | 1     |
      | page[size]   | 9     |
    Then validate the '200' status