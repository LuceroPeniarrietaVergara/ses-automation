package core;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import java.util.Map;
import static io.restassured.RestAssured.given;
import static org.junit.Assert.*;

public class Api {
    private Response response;
    private RequestSpecification request;
    private  String EVA_API = "http://webapi-uat.evapro.online/uat/api/v1/%s";
    private String TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJJZCI6IjE2IiwiTmFtZSI6Ikx1Y2VybyIsIkxhc3ROYW1lIjoiUGXDsWFycmlldGEgVmVyZ2FyYSIsIkVtYWlsIjoibHVjZXJvLnBlbmlhcnJpZXRhQGdtYWlsLmNvbSIsIlJvbGUiOiJBRE1JTiIsIlVzZXJuYW1lIjoiTHVjZXJvUGVuYXJyaWV0YVZlcmdhcmEiLCJBdmF0YXIiOiJodHRwczovL2xoNi5nb29nbGV1c2VyY29udGVudC5jb20vLWpsOW1LWnoxdzlzL0FBQUFBQUFBQUFJL0FBQUFBQUFBQTZRLzIzOE5IX1RoWTZVL3M1MC9waG90by5qcGciLCJDb25maXJtZWRBY2NvdW50IjoiVHJ1ZSIsImlzcyI6IkphbGFzb2Z0IiwiYXVkIjpbIkphbGFzb2Z0IiwiSmFsYXNvZnQiXSwiZXhwIjoxNTkzNzA5NDgzLCJQZXJtaXNzaW9uIjpbInByZXNlbnRhdGlvbjoxNSIsImV2YWx1YXRpb246MTUiLCJ0ZW1wbGF0ZToxNSIsInNjaGVtYToxNSIsInVzZXI6MTUiLCJyb2xlOjE1IiwiaW52aXRlOjE1IiwibXl0ZXN0OjE1Iiwic2NvcmU6MTUiLCJhbnN3ZXI6MTUiXX0.tWlR4FRi0In2haACdDKC2z4dvHMKEYmkd5Fu9kMDvOU";

    public Api(){
        request = given().header("Authorization", "Bearer " + TOKEN);
    }

    public void Get(String feature, Map<String, String> params){
        response = request.params(params)
                          .when()
                          .get(String.format(EVA_API, feature));
    }

    public void Validate(int status){
        //System.out.println("response: " + response.prettyPrint());
        //assertEquals(response.statusCode(), status);
    }
}
