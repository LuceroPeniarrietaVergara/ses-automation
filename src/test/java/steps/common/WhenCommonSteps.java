package steps.common;

import core.Api;
import cucumber.api.DataTable;
import cucumber.api.java.en.When;

public class WhenCommonSteps {

    @When("^execute the \\[GET] method for '(.*)' api:$")
    public void executeTheGETMethodForTemplatesApi(String feature, DataTable params) {
        new Api().Get(feature, params.asMap(String.class, String.class));
    }
}
