package steps.templates;

import core.Api;
import cucumber.api.java.en.Then;

public class ThenTemplatesSteps {
    @Then("^validate the '(\\d+)' status$")
    public void validateTheStatus(int status) {
        new Api().Validate(status);
    }
}
